import sys
import argparse

import cv2
print(cv2.__version__)

def extractImages(pathIn, pathOut):
    count = 0
    vidcap = cv2.VideoCapture(pathIn)
    while True:
        tail = f'{count:05}'
        success,image = vidcap.read()
        print ('Read a new frame: ', success)
        if success==True:
          cv2.imwrite( pathOut + tail + '.jpg', image)     # save frame as JPEG file
          count = count + 1
        else:
          break

if __name__=="__main__":
    a = argparse.ArgumentParser()
    a.add_argument("--pathIn", help="path to video")
    a.add_argument("--pathOut", help="path to images")
    args = a.parse_args()
    print(args)
    extractImages(args.pathIn, args.pathOut)