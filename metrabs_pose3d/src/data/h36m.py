import functools
import itertools
import os
import os.path
import xml.etree.ElementTree

import numpy as np
import pandas as pd
import json
#os.environ["CDF_LIB"] = "/home/mcw/miniconda3/envs/metrabs/lib/CDF/"
#import spacepy.pycdf
import transforms3d

import cameralib
import data.datasets3d as ps3d
import paths
import util
from data.preproc_for_efficiency import make_efficient_example


@util.cache_result_on_disk(f'{paths.CACHE_DIR}/gaitiq.pkl', min_time="2020-11-02T21:30:43")
def make_h36m(
        train_subjects=(''), valid_subjects=(''), test_subjects=(''),
        correct_S9=True, partial_visibility=False):

    joint_names = ('head,head_s,head_c,head_t,\
                    lhip,lhip_s,lhip_c,lhip_t,\
                    lank,lank_s,lank_c,lank_t,\
                    lelb,lelb_s,lelb_c,lelb_t,\
                    lwri,lwri_s,lwri_c,lwri_t,\
                    lkne,lkne_s,lkne_c,lkne_t,\
                    lsho,lsho_s,lsho_c,lsho_t,\
                    pelv,pelv_s,pelv_c,pelv_t,\
                    rhip,rhip_s,rhip_c,rhip_t,\
                    rank,rank_s,rank_c,rank_t,\
                    relb,relb_s,relb_c,relb_t,\
                    rwri,rwri_s,rwri_c,rwri_t,\
                    rkne,rkne_s,rkne_c,rkne_t,\
                    rsho,rsho_s,rsho_c,rsho_t,\
                    neck,neck_s,neck_c,neck_t'.split(','))
                    
    #joint_names = ('head,\
    #                lhip,lhip_s,lhip_c,lhip_t,\
    #                lank,lank_s,lank_c,lank_t,\
    #                lelb,\
    #                lwri,\
    #                lkne,lkne_s,lkne_c,lkne_t,\
    #                lsho,\
    #                pelv,\
    #                rhip,rhip_s,rhip_c,rhip_t,\
    #                rank,rank_s,rank_c,rank_t,\
    #                relb,\
    #                rwri,\
    #                rkne,rkne_s,rkne_c,rkne_t,\
    #                rsho,\
    #                neck'.split(','))
        
    edges = (
        'head-neck-lsho-lelb-lwri,neck-rsho-relb-rwri,'
        'neck-pelv-lhip-lkne-lank,pelv-rhip-rkne-rank')
    joint_info = ps3d.JointInfo(joint_names, edges)

    if not util.all_disjoint(train_subjects, valid_subjects, test_subjects):
        raise Exception('Set of train, val and test subject must be disjoint.')

    # use last subject of the non-test subjects for validation
    train_examples = []
    test_examples = []
    valid_examples = []
    pool = util.BoundedPool(None, 120)

    if partial_visibility:
        dir_suffix = '_partial'
        further_expansion_factor = 1.8
    else:
        dir_suffix = '' if correct_S9 else 'incorrect_S9'
        further_expansion_factor = 1

    for i_subject in [*test_subjects, *train_subjects, *valid_subjects]:
        if i_subject in train_subjects:
            examples_container = train_examples
        elif i_subject in valid_subjects:
            examples_container = valid_examples
        else:
            examples_container = test_examples

        #frame_step = 5 if i_subject in train_subjects else 64
        frame_step = 1
        
        for activity_name, camera_id in itertools.product(get_activity_names(i_subject), range(4)):
            
            
            if camera_id != 1:
              continue
            if 'Str' not in activity_name:
                continue
                
            print(f'Processing Subject {i_subject} {activity_name} {camera_id}')
            
            image_relpaths, world_coords_all, bboxes, camera = get_examples(
                i_subject, activity_name, camera_id, frame_step=frame_step, correct_S9=correct_S9)
            
         
            prev_coords = None
            for image_relpath, world_coords, bbox in zip(
                    util.progressbar(image_relpaths), world_coords_all, bboxes):
                    
                prev_coords = world_coords
                activity_name = activity_name.split(' ')[0]
                ex = ps3d.Pose3DExample(
                    image_relpath, world_coords, bbox, camera, activity_name=activity_name)
                new_image_relpath = image_relpath.replace('h36m', f'h36m_downscaled{dir_suffix}')
                pool.apply_async(
                          make_efficient_example, (ex, new_image_relpath, further_expansion_factor),
                              callback=examples_container.append)
               
    print('Waiting for tasks...')
    pool.close()
    pool.join()
    print('Done...')
    train_examples.sort(key=lambda x: x.image_path)
    valid_examples.sort(key=lambda x: x.image_path)
    test_examples.sort(key=lambda x: x.image_path)
    
    
    return ps3d.Pose3DDataset(joint_info, train_examples, valid_examples, test_examples)


def correct_boxes(bboxes, path, world_coords, camera):
    """Three activties for subject S9 have erroneous bounding boxes, they are horizontally shifted.
    This function corrects them. Use --dataset=h36m-incorrect-S9 to use the erroneous annotation."""

    def correct_image_coords(bad_imcoords):
        root_depths = camera.world_to_camera(world_coords[:, -1])[:, 2:]
        bad_worldcoords = camera.image_to_world(bad_imcoords, camera_depth=root_depths)
        good_worldcoords = bad_worldcoords + np.array([-200, 0, 0])
        good_imcoords = camera.world_to_image(good_worldcoords)
        return good_imcoords

    if 'S9' in path and ('SittingDown 1' in path or 'Waiting 1' in path or 'Greeting.' in path):
        toplefts = correct_image_coords(bboxes[:, :2])
        bottomrights = correct_image_coords(bboxes[:, :2] + bboxes[:, 2:])
        return np.concatenate([toplefts, bottomrights - toplefts], axis=-1)

    return bboxes


def correct_world_coords(coords, path):
    """Three activties for subject S9 have erroneous coords, they are horizontally shifted.
    This corrects them. Use --dataset=h36m-incorrect-S9 to use the erroneous annotation."""
    if 'S9' in path and ('SittingDown 1' in path or 'Waiting 1' in path or 'Greeting.' in path):
        coords = coords.copy()
        coords[:, :, 0] -= 200
    return coords


def make_gaitiq_bbox(coords_2d_path,bbox_path):
    joints_2d = np.array(pd.read_csv(coords_2d_path))
    joints_2d = joints_2d[1:]
    num_frames = len(joints_2d)
    bbox_all = []
    for i in range(num_frames):    
        gt_2d_frame = joints_2d[i]
        gt_2d = []
        for i in range(0,len(gt_2d_frame),2):
            gt_2d.append([float(gt_2d_frame[i]),float(gt_2d_frame[i+1])])
        
        gt_2d = np.array(gt_2d)

        min_x = min(gt_2d[:,0:1])
        max_x = max(gt_2d[:,0:1])        
        min_y = min(gt_2d[:,1:2])
        max_y = max(gt_2d[:,1:2])
        
        height = max_y - min_y
        width  =  max_x - min_x
        
        bbox = [int(min_x),int(min_y),int(width),int(height)]
        bbox_all.append(bbox)
    
    bbox_all = np.array(bbox_all)
    np.savez(bbox_path,bbox=bbox_all)
    

def get_examples(i_subject, activity_name, i_camera, frame_step=5, correct_S9=True):
    camera_names = ['Camera1', 'Camera2', 'Camera3', 'Camera4']
    camera_name = camera_names[i_camera]
    gaitiq_root = f'{paths.DATA_ROOT}'
    #camera = get_cameras(f'{gaitiq_root}/Release-v1.2/metadata.xml')[i_camera][i_subject - 1]
    
    camera_path = f'{gaitiq_root}/Subject {i_subject}/{activity_name}/{camera_name}/calibration.json'
    if i_subject ==0:
        camera = get_cameras_inference()
    else:
        camera = get_cameras(camera_path)
        
    def load_coords(coord_path):
        joints_3d_csv = np.array(pd.read_csv(coord_path))
        joints_3d_csv = joints_3d_csv[1:]
        
        joints_3d_world = []
        num_frames = len(joints_3d_csv)
        num_keypoints = len(joints_3d_csv[0])
        
        for frame in range(num_frames):
          temp_frame = []
          for i in range(0,num_keypoints,3):
            temp_frame.append([float(joints_3d_csv[frame][i]), float(joints_3d_csv[frame][i+1]), float(joints_3d_csv[frame][i+2])])  
          joints_3d_world.append(temp_frame)
        
          
        joints_3d_world = np.array(joints_3d_world)
        joints_3d_world =  joints_3d_world[::frame_step]
        return num_frames,joints_3d_world
    
    def load_coords_particular_keypoints(coord_path):
        #print(coord_path)
        joints_3d_csv = np.array(pd.read_csv(coord_path,header=None))        
        joints_3d_world = []
        num_frames = len(joints_3d_csv)
        num_keypoints = len(joints_3d_csv[0])
        #joint_names = ('head,head_s,head_c,head_t,\
        #                lhip,lhip_s,lhip_c,lhip_t,\
        #                lank,lank_s,lank_c,lank_t,\
        #                lelb,lelb_s,lelb_c,lelb_t,\
        #                lwri,lwri_s,lwri_c,lwri_t,\
        #                lkne,lkne_s,lkne_c,lkne_t,\
        #                lsho,lsho_s,lsho_c,lsho_t,\
        #                pelv,pelv_s,pelv_c,pelv_t,\
        #                rhip,rhip_s,rhip_c,rhip_t,\
        #                rank,rank_s,rank_c,rank_t,\
        #                relb,relb_s,relb_c,relb_t,\
        #                rwri,rwri_s,rwri_c,rwri_t,\
        #                rkne,rkne_s,rkne_c,rkne_t,\
        #                rsho,rsho_s,rsho_c,rsho_t,\
        #                neck,neck_s,neck_c,neck_t'.split(','))
        
        #selected_points = [0,4,5,6,7,8,9,10,11,12,16,20,21,22,23,24,28,32,33,34,35,36,37,38,39,40,44,48,49,50,51,52,56]
        
        for frame in range(num_frames):
          temp_frame = []
          for i in range(0,num_keypoints,3):
            #if int(i/3) not in selected_points:
            #    continue
            temp_frame.append([float(joints_3d_csv[frame][i]), float(joints_3d_csv[frame][i+1]), float(joints_3d_csv[frame][i+2])])  
          joints_3d_world.append(temp_frame)

        joints_3d_world = np.array(joints_3d_world)
        joints_3d_world =  joints_3d_world[::frame_step]
        return num_frames,joints_3d_world
        
    
    coord_path = f'{gaitiq_root}/Subject {i_subject}/{activity_name}/{camera_name}/orientation.csv'
    n_total_frames, world_coords = load_coords_particular_keypoints(coord_path)
    
    image_relfolder = f'{gaitiq_root}/Subject {i_subject}/{activity_name}/{camera_name}'
    
    image_relpaths = []
    for i_frame in range(0, n_total_frames, frame_step):
      image_path = f'{image_relfolder}/{i_frame:05d}.jpg'
      if os.path.exists(image_path):
        image_relpaths.append(image_path)
    
    coord_path_2d = f'{gaitiq_root}/Subject {i_subject}/{activity_name}/{camera_name}/joints_2d.csv'
    
    bbox_path = f'{gaitiq_root}/Subject {i_subject}/{activity_name}/{camera_name}/bbox.npz'
    make_gaitiq_bbox(coord_path_2d,bbox_path)
    
    if i_subject==0:
        print("-----------------------------------------------------------------------------------------------")
        bboxes = np.load("/home/mcw-nn/Nishanth/New/metrabs/src/YOLO/person_bbox.npz",allow_pickle=True)['bbox']
    else:
        bboxes = np.load(bbox_path,allow_pickle=True)['bbox']
    
    bboxes = bboxes[::frame_step]
    
    return image_relpaths, world_coords, bboxes, camera

def quaternion_rotation_matrix(Q):
    q0 = Q[0]
    q1 = Q[1]
    q2 = Q[2]
    q3 = Q[3]
    # First row of the rotation matrix
    r00 = 2 * (q0 * q0 + q1 * q1) - 1
    r01 = 2 * (q1 * q2 - q0 * q3)
    r02 = 2 * (q1 * q3 + q0 * q2)
    # Second row of the rotation matrix
    r10 = 2 * (q1 * q2 + q0 * q3)
    r11 = 2 * (q0 * q0 + q2 * q2) - 1
    r12 = 2 * (q2 * q3 - q0 * q1)
    # Third row of the rotation matrix
    r20 = 2 * (q1 * q3 - q0 * q2)
    r21 = 2 * (q2 * q3 + q0 * q1)
    r22 = 2 * (q0 * q0 + q3 * q3) - 1
    # 3x3 rotation matrix
    rot_matrix = np.array([[r00, r01, r02],
                           [r10, r11, r12],
                           [r20, r21, r22]])
    return rot_matrix

@functools.lru_cache()

def get_cameras(calib_path):
    f = open(calib_path)
    calib = json.load(f)
    
    translate = [calib['Translation']['x'],calib['Translation']['y'],calib['Translation']['z']]
    t = np.array(translate)
    
    
    f = [calib['Focal length'],calib['Focal length']]
    c = [calib['Principal point']['x'], calib['Principal point']['y']]
    intrinsic_matrix = np.array([
        [f[0], 0, c[0]],
        [0, f[1], c[1]],
        [0, 0, 1]], np.float32)
        
    
    Rotation 		= 	calib['Rotation']
    R = quaternion_rotation_matrix([ Rotation['w'],Rotation['x'],Rotation['y'],Rotation['z']])
    
    k = [calib['Radial distortion']['w0'], calib['Radial distortion']['w1'], calib['Radial distortion']['w2']]
    p = [0.0,0.0]  # Tangential distortion
    distortion_coeffs = np.array([k[0], k[1], p[0], p[1], k[2]], np.float32)
    
    return cameralib.Camera(t, R, intrinsic_matrix, distortion_coeffs)

def get_cameras_inference():
    
    translate = [0,0,0]
    t = np.array(translate)
    
    
    f = [383.487, 383.487]
    c = [321.143, 241.964]
    
    intrinsic_matrix = np.array([
        [f[0], 0, c[0]],
        [0, f[1], c[1]],
        [0, 0, 1]], np.float32)
        
    R = quaternion_rotation_matrix([0.0,0.0,0.0,0.0])
    
    k = [0.0,0.0,0.0]
    p = [0.0,0.0]  # Tangential distortion
    distortion_coeffs = np.array([k[0], k[1], p[0], p[1], k[2]], np.float32)
    
    return cameralib.Camera(t, R, intrinsic_matrix, distortion_coeffs)
    
def get_activity_names(i_subject):
    gaitiq_root = f'{paths.DATA_ROOT}'
    subject_images_root = f'{gaitiq_root}/Subject {i_subject}/'
    subdirs = [elem for elem in os.listdir(subject_images_root)
               if os.path.isdir(f'{subject_images_root}/{elem}')]
    activity_names = set(elem.split('.')[0] for elem in subdirs if '_' not in elem)
    return sorted(activity_names)


def generate_poseviz_gt(i_subject, activity_name, camera_id):
    camera_names = ['Camera1', 'Camera2', 'Camera3', 'Camera4']
    camera_name = camera_names[camera_id]
    image_relpaths, world_coords_all, bboxes, camera = get_examples(
        i_subject, activity_name, camera_id, frame_step=1, correct_S9=True)

    results = []
    examples = []
    for image_relpath, world_coords, bbox in zip(image_relpaths, world_coords_all, bboxes):
        results.append({
            'gt_poses': [world_coords.tolist()],
            'camera_intrinsics': camera.intrinsic_matrix.tolist(),
            'camera_extrinsics': camera.get_extrinsic_matrix().tolist(),
            'image_path': image_relpath,
            'bboxes': [bbox.tolist()]
        })
        ex = ps3d.Pose3DExample(
            image_relpath, world_coords, bbox, camera, activity_name=activity_name)
        examples.append(ex)

    joint_names = (
        'head,lhip,lank,lelb,lwri,lkne,lsho,pelv,rhip,rank,relb,rwri,rkne,rsho,neck'.split(','))
        
    edges = (
        'htop-head-neck-lsho-lelb-lwri,neck-rsho-relb-rwri,'
        'neck-pelv-lhip-lkne-lank,pelv-rhip-rkne-rank')
    joint_info = ps3d.JointInfo(joint_names, edges)
    ds = ps3d.Pose3DDataset(joint_info, test_examples=examples)
    util.dump_pickle(
        ds, f'{paths.DATA_ROOT}/GaitIQ/poseviz/Subject{i_subject}_{activity_name}_{camera_name}.pkl')

    output = {}
    output['joint_names'] = joint_info.names
    output['stick_figure_edges'] = joint_info.stick_figure_edges
    output['world_up'] = camera.world_up.tolist()
    output['frame_infos'] = results
    util.dump_json(
        output, f'{paths.DATA_ROOT}/GaitIQ/poseviz/Subject{i_subject}_{activity_name}_{camera_name}.json')
