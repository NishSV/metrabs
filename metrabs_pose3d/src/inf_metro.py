import tensorflow as tf
import cv2
import argparse
import glob
from matplotlib.lines import Line2D
from tqdm import tqdm
import numpy as np
import pandas as pd
import csv
import os
import matplotlib.pyplot as plt
plt.switch_backend('TkAgg')
from mpl_toolkits.mplot3d import Axes3D


model_path = "/home/mcw/Nishanth/New/metrabs/src/sp/"
detector_path = "/home/mcw/Nishanth/New/metrabs/src/YOLO/"
image_path_original ="/home/mcw/Nishanth/New/metrabs/00190.jpg"



command = 'cp ' + "\"" + image_path_original + "\"" + ' ' + '/home/mcw/Nishanth/New/metrabs/src/inference/input_image.jpg'
os.system(command)
image_path = '/home/mcw/Nishanth/New/metrabs/src/inference/input_image.jpg'

os.chdir(detector_path)
command = 'python3 detect.py --weights ./checkpoints/yolov4-416 --size 416 --model yolov4 --image ' + image_path
os.system(command)

bboxs = np.load('person_bbox.npz',allow_pickle=True)['bbox']
os.system('rm person_bbox.npz')
image = cv2.imread(image_path)
input_image = image

image_h, image_w, _ = image.shape
bounding_boxes = []
for i in range(len(bboxs)):
    coor = bboxs[i]
    coor[0] = int(coor[0] * image_h)
    coor[2] = int(coor[2] * image_h)
    coor[1] = int(coor[1] * image_w)
    coor[3] = int(coor[3] * image_w)
    
    c1, c2 = (coor[1], coor[0]), (coor[3], coor[2])
    
    x_min = coor[1]
    y_min = coor[0]
    x_max = coor[3]
    y_max = coor[2]
    
    height = y_max - y_min
    width = x_max - x_min
    
    bbox = [int(x_min), int(y_min), int(width), int(height)]
    bounding_boxes.append(bbox)
    print(bbox)

image = cv2.imread(image_path)
image = tf.image.crop_to_bounding_box(image,bounding_boxes[0][1],bounding_boxes[0][0],bounding_boxes[0][3],bounding_boxes[0][2])
image = tf.image.resize(image, [256,256] , preserve_aspect_ratio=False,antialias=False, name=None)
image = tf.cast(image, tf.uint8)
tf.keras.preprocessing.image.save_img('/home/mcw/Nishanth/New/metrabs/src/inference/cropped_image.jpg',image)
image = [image]
image = tf.cast(image, tf.float16)/255

crop_model = tf.saved_model.load(model_path)
poses = crop_model(image)#,intrinsic_matrix=None,boxes=bounding_boxes)

poses3d_pred = poses
poses3d_pred = poses3d_pred[0]
print(poses3d_pred)
joint_names = ('head,head_s,head_c,head_t,\
                    lhip,lhip_s,lhip_c,lhip_t,\
                    lank,lank_s,lank_c,lank_t,\
                    lelb,lelb_s,lelb_c,lelb_t,\
                    lwri,lwri_s,lwri_c,lwri_t,\
                    lkne,lkne_s,lkne_c,lkne_t,\
                    lsho,lsho_s,lsho_c,lsho_t,\
                    pelv,pelv_s,pelv_c,pelv_t,\
                    rhip,rhip_s,rhip_c,rhip_t,\
                    rank,rank_s,rank_c,rank_t,\
                    relb,relb_s,relb_c,relb_t,\
                    rwri,rwri_s,rwri_c,rwri_t,\
                    rkne,rkne_s,rkne_c,rkne_t,\
                    rsho,rsho_s,rsho_c,rsho_t,\
                    neck,neck_s,neck_c,neck_t'.split(','))
                    
joint_pairs = [ [0,56],[0,1],[0,2],[0,3],
                [56,0],[56,57],[56,58],[56,59], 
                [24,56],[24,25],[24,26],[24,27], 
                [12,24],[12,13],[12,14],[12,15], 
                [16,12],[16,17],[16,18],[16,19], 
                [52,56],[52,53],[52,54],[52,55],
                [40,52],[40,41],[40,42],[40,43],
                [44,40],[44,45],[44,46],[44,47],
                [28,56],[28,29],[28,30],[28,31],
                [4,28],[4,5],[4,6],[4,7],
                [20,4],[20,21],[20,22],[20,23],
                [8,20],[8,9],[8,10],[8,11],
                [32,28],[32,33],[32,34],[32,35],
                [48,32],[48,49],[48,50],[48,51],
                [36,48],[36,37],[36,38],[36,39] 
              ]
    

fig = plt.figure(figsize=(10, 5.2))

pose_ax = fig.add_subplot(1, 2, 2, projection='3d')
pose_ax.set_title('Prediction Orientation Keypoints')
image_ax = fig.add_subplot(1, 2, 1)
image_ax.set_title('Input')
image_ax.imshow(input_image)
#range_ = 1500
#pose_ax.view_init(98,91)
pose_ax.view_init(-87,-90)

#pose_ax.set_xlim3d(800, 1200)
#pose_ax.set_ylim3d(0,1600)
#pose_ax.set_zlim3d(800,1400)
pose_ax.set_box_aspect((1, 1, 1))

pose_ax.set_xlabel('X')
pose_ax.set_ylabel('Y')
pose_ax.set_zlabel('Z')

count = 0
for i_start, i_end in joint_pairs:
    if count%4 == 0: 
        pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='black')
    if count%4 == 1: 
        pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='green')
    if count%4 == 2: 
        pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='red')
    if count%4 == 3: 
        pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='blue')
    count+=1
    
pose_ax.scatter(poses3d_pred[:, 0], poses3d_pred[:, 1], poses3d_pred[:, 2], s=3,color='blue')

colors = ['black','green','red','blue']
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle="-") for c in colors]

labels = ['keypoints','s','c','t']
pose_ax.legend(lines, labels,loc='center left', bbox_to_anchor=(0.9, 0.02))

fig.tight_layout()
plt.savefig('/home/mcw/Nishanth/New/metrabs/src/inference/result_image.jpg')
plt.show()