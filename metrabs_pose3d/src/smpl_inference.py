import numpy as np
import os
import sys
import argparse
import cv2
import glob

if __name__ == "__main__":

  parser = argparse.ArgumentParser()

  parser.add_argument("--detector_path",help="YOLO path",default='/home/mcw-nn/Nishanth/New/metrabs/src/YOLO/')
  parser.add_argument("--image_path",required=True,help="input image")
  parser.add_argument("--logdir",required=True,help="pretrained model path")
  
  
  args = parser.parse_args()
  
  detector_path = args.detector_path
  image_path = args.image_path
  logdir = args.logdir
  
  os.chdir(detector_path)
  command = 'python3 detect.py --weights ./checkpoints/yolov4-416 --size 416 --model yolov4 --image ' + image_path
  os.system(command)
  
  bboxs = np.load('person_bbox.npz',allow_pickle=True)['bbox']
  
  image = cv2.imread(image_path)
  image_h, image_w, _ = image.shape
  bounding_boxes = []
  for i in range(len(bboxs)):
      coor = bboxs[i]
      coor[0] = int(coor[0] * image_h)
      coor[2] = int(coor[2] * image_h)
      coor[1] = int(coor[1] * image_w)
      coor[3] = int(coor[3] * image_w)
      
      c1, c2 = (coor[1], coor[0]), (coor[3], coor[2])
      
      x_min = coor[1]
      y_min = coor[0]
      x_max = coor[3]
      y_max = coor[2]
      
      height = y_max - y_min
      width = x_max - x_min
      
      bbox = [int(x_min), int(y_min), int(width), int(height)]
      bounding_boxes.append(bbox)
      print(bbox)
      
  np.savez('person_bbox.npz',bbox=bounding_boxes)
  
  cv2.imwrite('/home/mcw-nn/Nishanth/DATA/Subject 1/Straight 01/Camera2/00000.jpg',image)
  
  cache_dir = '/home/mcw-nn/Nishanth/DATA/cache/*'
  files = glob.glob(cache_dir)
  for f in files:
      os.remove(f)
  
  os.chdir('/home/mcw-nn/Nishanth/New/metrabs/src/')
  
  command = "python3 main.py --test --test-subjects '1' --occlude-aug-prob 0.0 --logdir "+ logdir +" --train-subjects '0' --valid-subjects '2'"
  
  os.system(command)
  
  command_vis = "python3 visualize.py --prediction_path "+logdir+'predictions_h36m.npz'
  
  os.system(command_vis)
  
  
  
  
  
  