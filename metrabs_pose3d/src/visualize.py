
import tensorflow as tf
import argparse
import glob
from matplotlib.lines import Line2D
from tqdm import tqdm
import numpy as np
import pandas as pd
import torch
import csv
import matplotlib.pyplot as plt
plt.switch_backend('TkAgg')
from mpl_toolkits.mplot3d import Axes3D
import cv2

def mpjpe(predicted, target):
    """
    Mean per-joint position error (i.e. mean Euclidean distance),
    often referred to as "Protocol #1" in many papers.
    """
    assert predicted.shape == target.shape
    return torch.mean(torch.norm(predicted - target, dim=len(target.shape) - 1))
    

def visualize(prediction_path):
    
    prediction = np.load(prediction_path,allow_pickle=True)
    
    image_paths = prediction['image_path']
    gt = prediction['coords3d_true']
    pred = prediction['coords3d_pred']
    
    evaluation = True
    
    gt_all = []
    pred_all = []
    img_all = []
    for i in range(len(gt)):
        pred_frame = pred[i]
        gt_frame = gt[i]
        
        poses_pred = pred_frame - pred_frame[28]
        poses_gt = gt_frame - gt_frame[28]
        
        #poses_pred = pred_frame
        #poses_gt = gt_frame
        
        pred_all.append(poses_pred)
        gt_all.append(poses_gt)

    
    joints_gt_all = []
    joints_pred_all = []
    gt_angle_all = []
    pred_angle_all = []
    for i in range(len(gt_all)):
        gt_frame = gt_all[i]
        pred_frame = pred_all[i]

        joints_gt_frame = []
        joints_pred_frame = []

        gt_angle_frame = []
        pred_angle_frame = []

        for j in range(len(gt_frame)):
            if j%4 == 0:
                joints_gt_frame.append(gt_frame[j])
                joints_pred_frame.append(pred_frame[j])
            else:
                gt_angle_frame.append(gt_frame[j])
                pred_angle_frame.append(pred_frame[j])
            
        #print(len(joints_gt_frame))
        joints_gt_all.append(joints_gt_frame)
        joints_pred_all.append(joints_pred_frame)        

        gt_angle_all.append(gt_angle_frame)
        pred_angle_all.append(pred_angle_frame)


    joints_gt_all = torch.Tensor(joints_gt_all)
    joints_pred_all = torch.Tensor(joints_pred_all)
    
    mpj = mpjpe(joints_pred_all,joints_gt_all)
    print("Joint Error : ",mpj) 

    gt_angle_all = torch.Tensor(gt_angle_all)
    pred_angle_all = torch.Tensor(pred_angle_all)
    
    mpj_angle = mpjpe(pred_angle_all,gt_angle_all)
    print("Angle Error : ",mpj_angle) 
    
    # mean and std



    # count = 0
    # ANGLE = [0,0,0]
    # for k in range(len(gt)):
    #     for i in range(len(gt[k])):
    #         count += 1
    #         for j in range(len(gt[k][i])):
    #             if (gt[k][i][j]) > 0.5 and (pred[k][i][j]) < -0.5:
    #                 ANGLE[j] += abs((1 - (gt[k][i][j])) - (-1 - (pred[k][i][j])))
    #             elif (gt[k][i][j]) < -0.5 and (pred[k][i][j]) > 0.5:
    #                 ANGLE[j] += abs((-1-(gt[k][i][j])) -( 1 - (pred[k][i][j])))
    #             else:
    #                 ANGLE[j] += abs((gt[k][i][j]) - (pred[k][i][j]))

    # meanS = ANGLE[0]/count
    # meanC = ANGLE[1]/count
    # meanT = ANGLE[2]/count
    # meanA = (meanT+meanC+meanS)/3
    
    # print("MAE OVERALL \t",meanA*180)
    # print("MAE SAGGITAL \t",meanS*180)
    # print("MAE CORONAL \t",meanC*180)
    # print("MAE TRANSVERSAL\t",meanT*180)

    pred_all = torch.Tensor(pred_all)
    gt_all = torch.Tensor(gt_all)
    
    mpj_all = mpjpe(pred_all,gt_all)
    print("Error : ",mpj_all) 
    

    
    print("Shape all : ",np.array(pred_all).shape)
    
    # for a particular frame
    
    index = 300
    poses3d_pred = pred_all[index]
    poses3d_gt = gt_all[index]
    image_path = image_paths[index].decode("utf-8") 
    print("Image Path : ",image_path)

    input_image = cv2.imread(image_path)

    #print('GT : \n',poses3d_gt)
    #print('Pred : \n',poses3d_pred)

    joint_names = ('head,head_s,head_c,head_t,\
                    lhip,lhip_s,lhip_c,lhip_t,\
                    lank,lank_s,lank_c,lank_t,\
                    lelb,lelb_s,lelb_c,lelb_t,\
                    lwri,lwri_s,lwri_c,lwri_t,\
                    lkne,lkne_s,lkne_c,lkne_t,\
                    lsho,lsho_s,lsho_c,lsho_t,\
                    pelv,pelv_s,pelv_c,pelv_t,\
                    rhip,rhip_s,rhip_c,rhip_t,\
                    rank,rank_s,rank_c,rank_t,\
                    relb,relb_s,relb_c,relb_t,\
                    rwri,rwri_s,rwri_c,rwri_t,\
                    rkne,rkne_s,rkne_c,rkne_t,\
                    rsho,rsho_s,rsho_c,rsho_t,\
                    neck,neck_s,neck_c,neck_t'.split(','))

    joint_pairs = [ [0,56],[0,1],[0,2],[0,3],
                [56,0],[56,57],[56,58],[56,59], 
                [24,56],[24,25],[24,26],[24,27], 
                [12,24],[12,13],[12,14],[12,15], 
                [16,12],[16,17],[16,18],[16,19], 
                [52,56],[52,53],[52,54],[52,55],
                [40,52],[40,41],[40,42],[40,43],
                [44,40],[44,45],[44,46],[44,47],
                [28,56],[28,29],[28,30],[28,31],
                [4,28],[4,5],[4,6],[4,7],
                [20,4],[20,21],[20,22],[20,23],
                [8,20],[8,9],[8,10],[8,11],
                [32,28],[32,33],[32,34],[32,35],
                [48,32],[48,49],[48,50],[48,51],
                [36,48],[36,37],[36,38],[36,39] 
              ]
    

    fig = plt.figure(figsize=(10, 5.2))

    pose_ax = fig.add_subplot(1, 2, 2, projection='3d')
    pose_ax.set_title('Prediction Orientation Keypoints')
    image_ax = fig.add_subplot(1, 2, 1)
    image_ax.set_title('Input')
    image_ax.imshow(input_image)
    #range_ = 1500
    #pose_ax.view_init(98,91)
    pose_ax.view_init(-87,-90)

    #pose_ax.set_xlim3d(800, 1200)
    #pose_ax.set_ylim3d(0,1600)
    #pose_ax.set_zlim3d(800,1400)
    pose_ax.set_box_aspect((1, 1, 1))

    pose_ax.set_xlabel('X')
    pose_ax.set_ylabel('Y')
    pose_ax.set_zlabel('Z')

    count = 0
    for i_start, i_end in joint_pairs:
        if count%4 == 0: 
            pose_ax.plot(*zip(poses3d_gt[i_start], poses3d_gt[i_end]), markersize=2,color='yellow')
        if count%4 == 1: 
            pose_ax.plot(*zip(poses3d_gt[i_start], poses3d_gt[i_end]), markersize=2,color='darkred')
        if count%4 == 2: 
            pose_ax.plot(*zip(poses3d_gt[i_start], poses3d_gt[i_end]), markersize=2,color='blue')
        if count%4 == 3: 
            pose_ax.plot(*zip(poses3d_gt[i_start], poses3d_gt[i_end]), markersize=2,color='darkgreen')
        count+=1
        
    pose_ax.scatter(poses3d_pred[:, 0], poses3d_pred[:, 1], poses3d_pred[:, 2], s=3,color='yellow')

    count = 0
    for i_start, i_end in joint_pairs:
        if count%4 == 0: 
            pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='black')
        if count%4 == 1: 
            pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='lightcoral')
        if count%4 == 2: 
            pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='deepskyblue')
        if count%4 == 3: 
            pose_ax.plot(*zip(poses3d_pred[i_start], poses3d_pred[i_end]), markersize=2,color='lightgreen')
        count+=1
        
    pose_ax.scatter(poses3d_pred[:, 0], poses3d_pred[:, 1], poses3d_pred[:, 2], s=3,color='black')

    colors = ['yellow','black','darkred', 'lightcoral', 'blue', 'deepskyblue', 'darkgreen','lightgreen']
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle="-") for c in colors]
    labels = ['joints GT','joints Prediction','Sagittal GT', 'Sagittal Pred', 'Coronal GT','Coronal Pred', 'Transverse GT', 'Transverse Pred']
    pose_ax.legend(lines, labels,loc='center left', bbox_to_anchor=(1.2, 0.1))

    fig.tight_layout()
    plt.savefig('/home/mcw/Nishanth/New/metrabs/src/inference/result_image.jpg')
    plt.show()

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--prediction_path',default=None,type=str)  
    
    args = parser.parse_args()
    visualize(**args.__dict__)




